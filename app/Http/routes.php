<?php

//Route::group(array('namespace' => 'Admin'), function()
//{
//	Route::get('/admin', array('as' => 'admin', 'uses' => 'LoginController@index'));
//});
Route::group(['middleware' => 'web'], function () {

    Route::get('/', 'HomeController@index');

    Route::auth();
    
    Route::get('/book', 'BookController@show');
    Route::get('book/{name}', 'BookController@showbookdetails');
    Route::get('/bookupload', 'BookController@upload');
    Route::post('/book', 'BookController@store');
    Route::get('/download/{id?}', 'BookController@getDownload');

    //Route::post('/bookupload', 'BookController@addBook');

    Route::get('/publishers' , 'BookController@showPub');
    Route::post('/publishers', 'BookController@storepub');

    Route::get('/categories' , 'BookController@showCat');
    Route::post('/categories', 'BookController@storecat');

    
});