<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Books;
use App\BookPublishers;
use App\BookCategory;
use Input;
use Validator;
use Response;
class BookController extends Controller
{
    public function __construct()
 	{
      $this->middleware('auth', ['except' => ['show', 'showbookdetails']]);
 	}

    public function upload()
    {
    	return view('books.form');
    }

    public function show()
    {
    	$books = Books::all();
        return view('books.show')->with('books',$books);
    }

    public function showbookdetails($name)
    {
        $book = Books::getbook($name);
        
        $publisher = BookPublishers::getpub($book->publisher_id);
        
        $category = BookCategory::getcat($book->category_id);
        
        return view('books.showbookdetails')->with(compact('book'))->with(compact('publisher'))->with(compact('category'));
    }

    public function getDownload($id)
    {
            //$file= public_path(). '/books/uploads/';
            $file = Books::findOrFail($id);
            //dd($file);
            $path = $file->path;
            $name = $file->book_temp_name;
            //dd($name);
            return Response::download($path, $name);
            
            //return redirect()->back();
    }

    public function store(Request $request)
    {
        $bookfile = array('book_temp_name' => Input::file('book_temp_name'));
        $rules = array('book_temp_name' => 'required');
        $validator = Validator::make($bookfile, $rules);
        if($validator->fails())
        {
            echo "false";
        }
        else
        //if(Input::file('book_file_name')->isValid())
        {
            $extension = Input::file('book_temp_name')->getClientOriginalExtension();
            $fileName = sha1(time()). '.' .$extension;
            $destinationPath = 'books/uploads';
            $filePath = $destinationPath. '/' .$fileName;
            Input::file('book_temp_name')->move($destinationPath,$fileName);

            $pub_id = $request->input('publisher_id');
            $cat_id = $request->input('category_id');
            $typ_id = $request->input('type_id');
            $name = $request->input('name');
            $isbn = $request->input('isbn');
            $desc = $request->input('description');
        }
        $book = Books::create([
                'publisher_id' => $pub_id,
                'category_id'=>$cat_id,
                'type_id'=>$typ_id,
                'name'=>$name,
                'isbn'=>$isbn,
                'description' =>$desc,
                'book_temp_name' => $fileName,
                'path' => $filePath,
                ]);
        $book->save();
        return redirect('book');
    }

    public function storepub(Request $request)
    {
        $BookPublishers = BookPublishers::create($request->all());
        return redirect('bookupload');
    }

    public function showPub()
    {
        $publishers = BookPublishers::all();
        return view('publishers.show')->with('publishers',$publishers);
    }

    public function storecat(Request $request)
    {
        $BookCategory = BookCategory::create($request->all());
        return redirect('bookupload');
    }

    public function showCat()
    {
        $categories = BookCategory::all();
        return view('categories.show')->with('categories',$categories);
    }
}
