<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookCategory extends Model
{
    protected $table = 'book_category';

    public $timestamps = false;

    protected $fillable = ['category'];

    public function books()
    {
    	return $this->hasMany('App\Books');
    }

    public static function getcat($id)
    {
    	return static::where('id', $id)->firstOrFail();
    }
}
