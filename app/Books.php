<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class Books extends Model
{
    protected $table = 'books';

    //public $timestamps = false;

    protected $fillable = ['publisher_id', 'category_id', 'type_id' ,'name', 'isbn', 'description', 'book_temp_name', 'path'];

    protected $file;

    public static function getbook($name) 
    {
        //$name = str_replace('-', ' ', $name);
        return static::where('name', $name)->firstOrFail();
    }
    
    public function book_types()
    {
    	return $this->belongsTo('App\BookTypes');
    }

    public function book_publishers()
    {
    	return $this->belongsTo('App\BookPublishers' , 'publisher_id');
    }

    public function book_category()
    {
    	return $this->belongsTo('App\BookCategory');
    }

}
