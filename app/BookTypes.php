<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookTypes extends Model
{
    protected $table = 'book_types';

    public $timestamps = false;

    protected $fillable = ['book_type'];

    public function books()
    {
    	return $this->hasMany('App\Books');
    }
}
