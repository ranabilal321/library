<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Books;

class Delete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete {--id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'deletes the file by their id like delete --id=1';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $book = DB::table('books')->where('id',$this->option('id'))->get();
        if($book[0]->id == $this->option('id'))
        {
            $book = DB::table('books')->where('id',$this->option('id'))->delete();
            echo "Book Deleted Successfully!";
        }
    }
}
