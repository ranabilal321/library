<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Books;
use App\BookTypes;
use DB;

class ListBooks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'list:books {--type=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Display all the books list:books, by specific category list:books --type=pdf.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(!$this->option('type'))
        {
            $books = Books::all('id', 'name')->toArray();
            $headers = ['ID','Name'];
            $this->table($headers, $books);
        }        

        else
        {
            $typ = DB::table('book_types')->where('book_type',$this->option('type'))->get();

            $type_id = $typ[0]->id;

            $results1 = DB::table('books')->where('type_id',$type_id)->get();
        
            $results2=[];
        
            $count=0;
        
            foreach($results1 as $key => $value) {

                $results2[$count]=[
                    'name' => $value->name,
                    //'type' => $this->option('type')
                ];
                $count++;
            }

            $results2 = json_decode(json_encode($results2), true);
            $headers = ['Books'];        
            $this->table($headers, $results2);
        }
    }
}
