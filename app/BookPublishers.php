<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookPublishers extends Model
{
    protected $table = 'book_publishers';

    public $timestamps = false;

    protected $fillable = ['publishers'];

    public function books()
    {
    	return $this->hasMany('App\Books');
    }

    public static function getpub($id)
    {
    	return static::where('id', $id)->firstOrFail();
    }
}
