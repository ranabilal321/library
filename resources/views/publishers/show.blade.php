@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
				<div class="panel-heading">Publishers List.</div>
					<div class="panel-body">
					@foreach($publishers as $publisher)
    					<ul>
        					{!!$publisher->id !!}
        					{!!$publisher->publishers !!}
    					</ul>
					@endforeach
					<form method = "POST" action = "publishers" enctype = "multipart/form-data">
						{{csrf_field()}}
							<div class ="form-group">
								<div class = "col-md-5">
									<hr>
									<label for ="publishers">Publisher Not On The List?Add New Publisher:</label>
									<input type ="text" name ="publishers" id="publishers" class ="form-control" value ="" required>
									<hr>
									<button type ="submit"class ="btn btn-primary">Add Publisher</button>
								</div>
							</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop