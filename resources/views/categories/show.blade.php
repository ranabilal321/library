@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
				<div class="panel-heading">Categories List.</div>
					<div class="panel-body">
					@foreach($categories as $category)
    					<ul>
        					{!!$category->id !!}
        					{!!$category->category !!}
    					</ul>
					@endforeach
					<form method = "POST" action = "categories" enctype = "multipart/form-data">
						{{csrf_field()}}
							<div class ="form-group">
								<div class = "col-md-5">
									<hr>
									<label for ="category">Category Not On The List?Add New Category:</label>
									<input type ="text" name ="category" id="category" class ="form-control" value ="" required>
									<hr>
									<button type ="submit"class ="btn btn-primary">Add Category</button>
								</div>
							</div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop