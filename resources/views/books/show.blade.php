@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
				<div class="panel-heading">Click on the Book for all information.</div>
					<div class="panel-body">
					@foreach($books as $book)
    					<li>
    						<a href="/book/{{$book->name }}">
        					<h4>{!!$book->name !!}</h1>
    					</li>
					@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop