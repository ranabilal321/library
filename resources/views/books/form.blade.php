@extends('layouts.app')

@section('content')
<form method = "POST" action = "book" enctype = "multipart/form-data">
<div class = "row">
	{{csrf_field()}}
	<div class = "col-lg-12"> 
		<div class = "col-lg-4"></div>
		<div class = "col-lg-4"><h1>Upload Book!</h1></div>
		<div class = "col-lg-4"></div>
	</div>
	<div style="clear:both"></div>

	<div class = "col-lg-12">
		<div class = "col-lg-2"> </div> 
		<div class = "col-lg-4">
			<div class ="form-group">
				<label for ="name">Book Name:</label>
				<input type ="text" name ="name" id="name" class ="form-control" value ="" required>
			</div>		
		
			<div class ="form-group">
				<label for ="isbn">ISBN:</label>
				<input type ="text" name ="isbn" id="isbn" class ="form-control" value ="" required>
			</div>

			<div class ="form-group">
				<label for ="description">Book Description:</label>
				<textarea type ="text" name ="description" id="description" class ="form-control" rows = "10" value ="" required></textarea>
			</div>
		</div>
	
		<div class = "col-lg-4">

			<div class ="form-group">
				<label for ="publisher_id">Publishers:</label>
				<select id ="publisher_id" name ="publisher_id" class="form-control" value ="">
					@foreach(App\BookPublishers::all() as $publisher)
					<option value = "{{ $publisher->id }}">{{$publisher->publishers}}</option>
					@endforeach
				</select>

				<a href="{{ url('/publishers') }}">View Publishers</a>
			</div>

			<div class ="form-group">
				<select id ="category_id" name ="category_id" class="form-control" value ="">
					@foreach(App\BookCategory::all() as $category)
					<option value = "{{ $category->id }}">{{$category->category}}</option>
					@endforeach
				</select>

				<a href="{{ url('/categories') }}">View Categories</a>
			</div>

			<div class ="form-group">
				<label for ="type_id">Types:</label>
				<select id ="type_id" name ="type_id" class="form-control" value ="">
					@foreach(App\BookTypes::all() as $type)
					<option value = "{{ $type->id }}">{{$type->book_type}}</option>
					@endforeach
				</select>
			</div>

			<div class ="form-group">
				<input type="file" name="book_temp_name" id = "book_temp_name" accept = "application/pdf" required>
			</div>

			<div class ="form-group">
				<button type ="submit"class ="btn btn-primary">Add Book</button>
			</div>
		</div>
		<div class ="col-lg-2"></div>
	</div>
</div>
</form>

@endsection