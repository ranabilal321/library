@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
				<div class="panel-heading">Book Details</div>
					<div class="panel-body">
						<h4>Book Name : </h4> {!!$book->name!!}
						<h4>Book ISBN : </h4> {!!$book->isbn!!}
						<h4>Book Publisher : </h4> {!!$publisher->publishers!!}
						<h4>Book Category  : </h4> {!!$category->category!!}
						<h4>Book Description : </h4> {!!$book->description!!}
						<div class ="form-group">
						<a href="/download/{{$book->id}}" class="btn btn-primary">Download</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop