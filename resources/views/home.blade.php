@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    Library app, click to view or add books.
                </div>
                <div class = "panel-body">
                    <a href = "/book"class="btn btn-primary">Show Books</a>
                    <a href = "/bookupload"class="btn btn-primary">Add Book</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
